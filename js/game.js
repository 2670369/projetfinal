/*
* Alexandre Vinette & Joël Paulin
* 14/04/2020
* game.js
* javascript for the gameplay part of the website
*/

/**
 * allows us to create the player 
 * @param {int} hp - player hit points
 * @param {*} block - player block points
 */
function createPlayer(hp, block) {
    var player = {};
    player.hp = hp;
    player.block = block;

    return player;
}

/**
 * allows us to create monsters as enemies for the player
 * @param {String} name - monster name
 * @param {int} hp - monster hit points
 */
function createMonster(name, hp) {
    var monster = {};
    monster.name = name;
    monster.hp = hp;
    monster.display = function(idleAnimation) {
    animation.classList.add(idleAnimation);
    }
    return monster;ldldldldl
}

/**
 *allows us to create cards for the player to use
 * @param {String} name - card name
 * @param {String} cName - class name
 * @param {int} attack - card damage
 * @param {int} block - card block
 */
function createCard(name, cName, attack, block){
    var card = {};
    card.name = name;
    card.cName = cName;
    card.attack = attack;
    card.block = block;
    return card;
}

//creating attack and block cards and placing it into a deck
var AttackCard = createCard("Attack", "AttackCard", 7, 0);
var BlockCard = createCard("Block", "BlockCard", 0, 5) ;
var drawPile = [AttackCard, AttackCard, AttackCard, AttackCard, AttackCard, AttackCard, AttackCard, AttackCard, BlockCard, BlockCard, BlockCard, BlockCard, BlockCard, BlockCard, BlockCard];
var discardPile = [];

/**
 * Draw a card randomly from our drawpile array
 */
function drawCard() {
   var cardIndex = Math.floor(Math.random() * drawPile.length);
   var card = drawPile[cardIndex];
   drawPile.splice(cardIndex, 1);
   return card;
}

//creating our player and monsters
var player = createPlayer(100, 0);
var skelly = createMonster('Skelly', 20);
var goblin = createMonster('Goblin', 20);
var bat = createMonster('Bat', 20);

//setting our base animation class
var animation = document.getElementById("monster");

//getting our card placement from html
var card1 = document.getElementById("card1");
var card2 = document.getElementById("card2");
var card3 = document.getElementById("card3");
var card4 = document.getElementById("card4");
var card5 = document.getElementById("card5");

//gets the lenght of the drawpile and displays it on top of the deck
var cardsLeft = document.getElementById("deck");
cardsLeft.innerHTML = drawPile.length;

/**
 * makes the card disapear from the hand once it is played
 * @param {createCard} card - refers to the card being played
 */
function checkCardsPlayed(card){
            card.style.display = "none";
    }

//Sets status bars
var enemyHealth = document.getElementById("enemyHealth");
var playerHealth = document.getElementById("playerHealth");
var playerBlock = document.getElementById("block");


/**
 * decrements the enemy's life bar
 */
function enemyLife(){
    var rand = Math.ceil(Math.random() * 30 + 10);
    var wide = enemyHealth.offsetWidth;
    if (rand >= wide) {
        enemyHealth.style.width = 0 + "px";
    }
    else{
    var newWide = wide - Math.ceil(Math.random() * 30 + 10);;
    enemyHealth.style.width = newWide + "px";
    }
}

/**
 * decrements player block bar, if at zero decrements player health
 */
function playHealth(){
    if (playerBlock.offsetWidth > 0){
        var wide = playerBlock.offsetWidth;
        var newWide = wide - 15;
        playerBlock.style.width = newWide + "px";
    }
    else {
    var rand = Math.ceil(Math.random() * 30 + 10);
    var wide = playerHealth.offsetWidth;
    if (rand >= wide){
        playerHealth.style.width = 0 + "px";
    }
    else{
    var newWide = wide - Math.ceil(Math.random() * 30 + 10);
    playerHealth.style.width = newWide + "px";
    }
}
}

/**
 * increments player block bar
 */
function playBlock(){
    var wide = playerBlock.offsetWidth;
    var newWide = wide + 15;
    playerBlock.style.width = newWide + "px";
}

var piger = document.getElementById("pige");

function discard(card){
    discardPile.push(card);

}

 function cardSelect(card){
        if (card.className == "AttackCard") {
            card.onclick = function() {a(card);}
         }

        else if (card.className == "BlockCard") {
            card.onclick = function() {b(card);}
        } 
    }

/**
 * draws five cards, assings them their css class
 * set their onclicks depending on card type
 * pushes the cards to discard pile for later reshuffeling
 * resets the card display to inline-block
 */
function drawHand(){

    var one = drawCard();
    var two = drawCard();
    var three = drawCard();
    var four = drawCard();
    var five = drawCard();

    card1.className = one.cName;
    card2.className = two.cName;
    card3.className = three.cName;
    card4.className = four.cName;
    card5.className = five.cName;

    cardSelect(card1);
    cardSelect(card2);
    cardSelect(card3);
    cardSelect(card4);
    cardSelect(card5);

    discardPile.push(one);
    discardPile.push(two);
    discardPile.push(three);
    discardPile.push(four);
    discardPile.push(five);

    if (discardPile.length == 15){
        for(var i = 0; i < discardPile.length; i++){
            drawPile.push(discardPile[i]);
        }
        discardPile = [];
    }

    cardsLeft.innerHTML = drawPile.length;

    card1.style.display = "inline-block";
    card2.style.display = "inline-block";
    card3.style.display = "inline-block";
    card4.style.display = "inline-block";
    card5.style.display = "inline-block";
}

function handlePiles(){
    discardPile.push(one);
    discardPile.push(two);
    discardPile.push(three);
    discardPile.push(four);
    discardPile.push(five);

    if (discardPile.length == 15){
        for(var i = 0; i > discardPile.length; i++){
            drawPile.push(discardPile[i]);
        }
        discardPile = [];
    }
}

//gets rid of remaining cards from the screen
function discardHand(){
    card1.style.display = "none";
    card2.style.display = "none";
    card3.style.display = "none";
    card4.style.display = "none";
    card5.style.display = "none";

}

//Picks a window title from a list
var title = ["Fight Time!", "Get Em'!", "Nyahh!!!", "Good luck...", "Pro tip: try to attack", "You are unstoppable", "Bring it!", "Rip and tear!", "For Glory!"];
window.document.title = title[Math.round(Math.random() * (title.length - 1))];

//button to end turn
var nextTurn = document.getElementById("joue");

// condition that picks the skeleton
if(Math.random() < 0.3333) {
   
    drawHand();
    
    //sets default animation to idle
    skelly.display("SkellyIdle");

    //sets listeners for end of animations
    animation.addEventListener("webkitAnimationEnd", animate);
    animation.addEventListener("animationend", animate);
    
    //sets click functions
    cardSelect(card1);
    cardSelect(card2);
    cardSelect(card3);
    cardSelect(card4);
    cardSelect(card5);
    
    //handles basic animation
    function animate(){
        if (!animation.className.includes("SkellyIdle")){
            animation.classList = "SkellyIdle";
        }
            
    }
    
    /**
     * function for if an attack card is played
     * @param {createCard} card - card being selected
     */
    function a(card){
        //trigger the Hit animation
        if (animation.classList.contains("SkellyIdle")){
            animation.classList.replace("SkellyIdle", "SkellyHit")
        } 

        enemyLife(card);

        //checks for a win state and discards card from hand
        if (enemyHealth.offsetWidth <= 0){
            skeletonDefeated = true;
            location.href = 'win.html';
        }
        checkCardsPlayed(card);
    }
     
    /**
     * function for if block card is played
     * @param {createCard} card - card being selected 
     */
     function b(card) {
        playBlock();
        checkCardsPlayed(card);
    }

    /**
     * When the player clicks the end turn button
     * sets the enemy to attack
     * checks for lose state
     * discards remaing hand and draws a new hand
     */
    function endTurn() {
        if (animation.classList.contains("SkellyIdle")){
            animation.classList.replace("SkellyIdle", "SkellyAttack")
        }
        playHealth();
        if (playerHealth.offsetWidth <= 0){
            location.href = 'loose.html';
        }
        discardHand();
        drawHand();
    }

    //set onlick for nextTurn button
    nextTurn.onclick = function() {endTurn();}
    
}

//condition that picks the goblin
//The everythin in this block works the same as for the skeleton
//the only difference is the animations
//this is why we had to repeat and redefine some of the functions.
else if(Math.random() < 0.6666) {

    drawHand();

    goblin.display("GoblinIdle");

    animation.addEventListener("webkitAnimationEnd", animate);
    animation.addEventListener("animationend", animate);
    
    cardSelect(card1);
    cardSelect(card2);
    cardSelect(card3);
    cardSelect(card4);
    cardSelect(card5);
     
    function animate(){
        if (!animation.className.includes("GoblinIdle")){
            animation.classList = "GoblinIdle";
        }
    }
    
    function a(card){
        if (animation.classList.contains("GoblinIdle")){
            animation.classList.replace("GoblinIdle", "GoblinHit")
        } 
        enemyLife();
        if (enemyHealth.offsetWidth <= 0){
            location.href = 'win.html';
        }
        checkCardsPlayed(card);
    }

     function b(card) {
        playBlock();
        checkCardsPlayed(card);
    }

    function endTurn() {
        if (animation.classList.contains("GoblinIdle")){
            animation.classList.replace("GoblinIdle", "GoblinAttack")
        }
        playHealth();
        if (playerHealth.offsetWidth <= 0){
            location.href = 'loose.html';
        }
        discardHand();
        drawHand();
    }
    nextTurn.onclick = function() {endTurn();}
}

//conditions that picks the bat
//everything from goblin and skeleton works the same
else {

    drawHand();

    bat.display("BatIdle");

    animation.addEventListener("webkitAnimationEnd", animate);
    animation.addEventListener("animationend", animate);
    
    cardSelect(card1);
    cardSelect(card2);
    cardSelect(card3);
    cardSelect(card4);
    cardSelect(card5);
     
    function animate(){
        if (!animation.className.includes("BatIdle")){
            animation.classList = "BatIdle";
        }
    }
    
    function a(card){
        if (animation.classList.contains("BatIdle")){
            animation.classList.replace("BatIdle", "BatHit")
        } 
        enemyLife();
        if (enemyHealth.offsetWidth <= 0){
            location.href = 'win.html';
        }
        checkCardsPlayed(card);
    }

    function b(card) {
        playBlock();
        checkCardsPlayed(card);
    }

    function endTurn() {
        if (animation.classList.contains("BatIdle")){
            animation.classList.replace("BatIdle", "BatAttack")
        }
        playHealth();
        if (playerHealth.offsetWidth <= 0){
            location.href = 'loose.html'
        }
        discardHand();
        drawHand();
    }

    nextTurn.onclick = function() {endTurn();}
}
